#ifndef BLUEZ_API_H
#define	BLUEZ_API_H

#ifdef	__cplusplus
extern "C" {
#endif

#define BLUEZ_DBUS_SERVICE_NAME "org.bluez"
#define BLUEZ_DBUS_BASE_PATH "/org/bluez"

// Still needed for helpers.c, probably will have to trim this one down too
#define BLUEZ_OBEX_DBUS_SERVICE_NAME "org.bluez.obex"
#define BLUEZ_OBEX_DBUS_BASE_PATH "/org/bluez/obex"

#include "manager.h"

#include "bluez/adapter.h"
#include "bluez/agent_manager.h"
#include "bluez/device.h"
    
#ifdef	__cplusplus
}
#endif

#endif	/* BLUEZ_H */

