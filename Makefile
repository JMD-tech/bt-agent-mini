
CFLAGS+= $(shell pkg-config --cflags glib-2.0)

LDFLAGS+= $(shell pkg-config --libs glib-2.0) -lgio-2.0 -lgobject-2.0 -lglib-2.0 -lsystemd

OBJS=bt-agent.o agent-helper.o dbus-common.o helpers.o manager.o properties.o agent_manager.o device.o adapter.o

VPATH=.:lib:lib/bluez:lib/bluez/obex

default: all

all: bt-agent

clean:
	rm -f *.o *.exe bt-agent-mini

bt-agent: $(OBJS)
	$(CC) $(CFLAGS) -o bt-agent-mini $(OBJS) $(LDFLAGS)

install: bt-agent
	install -d $(DESTDIR)/opt/bt-agent-mini/
	install -m 755 bt-agent-mini $(DESTDIR)/opt/bt-agent-mini/

install_service: install
	install -m 644 bt-agent-mini.service $(DESTDIR)/etc/systemd/system/
	systemctl daemon-reload
	#systemctl enable bt-agent-mini.service

