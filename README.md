## bt-agent-mini

A non-interactive simplistic bluetooth pairing agent, to be integrated into bluetooth devices based on Raspberry Pi or similar Linux based mini-systems.

Basically a forked extract of the bt-agent tool from [bluez tools](https://github.com/khvzak/bluez-tools), with following modifications:
* Getting rid of the autoconf/automake/automess nonsense, such a small and simple Linux-only tool compiles easier with only a simple Makefile on any decent Linux distro.
* Disable the keyboard confirmation for accepting connections.
* (temporary) Disable PIN-pairing (original wasn't working on our system, will have to test further).
* (temporary) Force NoInputNoOutput capabilities mode.
* Remove most source files and functions from Bluez that are unneeded for bt-agent compilation.
* Add a .service file to allow background operation.

Planned:
* Re-enable PIN-pairing.
* Add some kind of IPC (probably a Unix socket) to allow main application serving as human interface for screen/keyboard authentication modes, which allows:
* Re-enable support for other capabilities mode.

Build-dependencies:
* build-essential
* libglib2.0-dev
* libsystemd-dev

Compilation:
```
	make
```

Installation as a service:
```
	make install_service
	systemctl enable bt-agent-mini
	systemctl start bt-agent-mini
```

License:

Forked from GPLv2 code, stays as GPLv2 code.
