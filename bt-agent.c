/*
 *  bt-agent-mini - a simplistic non-interactive bluetooth pairing agent
 *
 *  Julien Marodon <jmarodon@jmd-tech.com>
 *
 *  forked from:
 *
 *  bluez-tools - a set of tools to manage bluetooth devices for linux
 *
 *  Copyright (C) 2010-2011  Alexander Orlenko <zxteam@gmail.com>
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <glib-unix.h>

#include "lib/dbus-common.h"
#include "lib/helpers.h"
#include "lib/agent-helper.h"
#include "lib/bluez-api.h"

#include <systemd/sd-daemon.h>

static gboolean need_unregister = TRUE;
static GMainLoop *mainloop = NULL;

static GHashTable *pin_hash_table = NULL;
//static gchar *pin_arg = NULL;

static gboolean
usr1_signal_handler(gpointer data)
{
	g_message("SIGUSR1 received");

	//if (!pin_arg)
		return G_SOURCE_CONTINUE;

	//g_print("Re-reading PIN's file\n");
	//_read_pin_file(pin_arg, pin_hash_table, FALSE);

	return G_SOURCE_CONTINUE;
}

static gboolean
term_signal_handler(gpointer data)
{
	if (g_main_loop_is_running(mainloop))
		g_main_loop_quit(mainloop);

	return G_SOURCE_REMOVE;
}

static gchar *capability_arg = NULL;

int main(int argc, char *argv[])
{
	GError *error = NULL;

	/* Query current locale */
	setlocale(LC_CTYPE, "");

	dbus_init();

	capability_arg = "NoInputNoOutput";

	if (!dbus_system_connect(&error))
	{
		g_printerr("Couldn't connect to DBus system bus: %s\n", error->message);
		exit(EXIT_FAILURE);
	}

	/* Check, that bluetooth daemon is running */
	if (!intf_supported(BLUEZ_DBUS_SERVICE_NAME, MANAGER_DBUS_PATH, MANAGER_DBUS_INTERFACE))
        {
		g_printerr("%s: bluez service is not found\n", g_get_prgname());
		g_printerr("Did you forget to run bluetoothd?\n");
		exit(EXIT_FAILURE);
	}
        
	// Read PIN's file 
	/*
	if (pin_arg)
	{
		pin_hash_table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
		_read_pin_file(pin_arg, pin_hash_table, TRUE);
	}
	*/

	mainloop = g_main_loop_new(NULL, FALSE);

	Manager *manager = g_object_new(MANAGER_TYPE, NULL);

	AgentManager *agent_manager = agent_manager_new();

	register_agent_callbacks(TRUE, pin_hash_table, mainloop, &error);
	
	exit_if_error(error);
	
	agent_manager_register_agent(agent_manager, AGENT_PATH, capability_arg, &error);
	exit_if_error(error);
	g_print("Agent registered\n");

	agent_manager_request_default_agent(agent_manager, AGENT_PATH, &error);
	exit_if_error(error);
	g_print("Default agent requested\n");

	/* Add SIGTERM/SIGINT/SIGUSR1 handlers */
	g_unix_signal_add (SIGTERM, term_signal_handler, NULL);
	g_unix_signal_add (SIGINT, term_signal_handler, NULL);
	g_unix_signal_add (SIGUSR1, usr1_signal_handler, NULL);

	sd_notify(0,"READY=1");

	g_main_loop_run(mainloop);

	if (need_unregister) {
		g_print("unregistering agent...\n");
		agent_manager_unregister_agent(agent_manager, AGENT_PATH, &error);
		exit_if_error(error);
	}

	g_main_loop_unref(mainloop);

	unregister_agent_callbacks(NULL);
	g_object_unref(agent_manager);
	g_object_unref(manager);

	dbus_disconnect();

	exit(EXIT_SUCCESS);
}
